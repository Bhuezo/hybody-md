import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex);

export const store = new Vuex.Store({
    state:{
        language: false,
        PRODUCTOS_ITEMS: [],
        PRODUCTOS_SEARCH: [],
    },
    mutations:{
        getSearch (state,search){
            state.PRODUCTOS_SEARCH = state.PRODUCTOS_ITEMS.filter(function (res){
                let el1 = res.producto.toLowerCase()
                if(search==='') return true
                else return el1.indexOf(search.toLowerCase()) > -1
            })
        },
        getCategoria (state,search){
            state.PRODUCTOS_SEARCH = state.PRODUCTOS_ITEMS.filter(function (res){
                let el1 = res.categoria.toLowerCase()
                if(search==='') return true
                else return el1.indexOf(search.toLowerCase()) > -1
            })
        },
        getAll(state){
            axios.defaults.baseURL = 'http://localhost/hybody/core/';
            axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';
            axios.defaults.headers.post['Access-Control-Allow-Origin'] = '*'
            axios.get('api/productos.php?privilege=public&action=leer')
            .then(res =>{
                if (res.data.status == 1 && res.data.productos != []) {
                    state.PRODUCTOS_ITEMS = res.data.productos
                    state.PRODUCTOS_SEARCH = state.PRODUCTOS_ITEMS
                    //this.verificarcarrito(sessionStorage.getItem('token'))
                    //console.log(res)
                }
                else {
                    console.log(res)
                }
            })
        },
        changeLanguaje(state){
            state.language = !state.language
        }
    },
    getters:{
        idioma(state){
            return state.language
        },
        productos(state){
            return state.PRODUCTOS_ITEMS
        },
        productosS(state){
            return state.PRODUCTOS_SEARCH
        }
    },
    actions:{
        changeLanguage({commit}){
            commit('changeLanguaje')
        },
        getProductos({commit}){
            commit('getAll')
        },
        searchProducto({commit}, search){
            commit('getSearch', search)
        },
        categoryProducto({commit}, search){
            commit('getCategoria', search)
        }
    }
})