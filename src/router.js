import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/carrito',
      name: 'carrito',
      component: () => import(/* webpackChunkName: "about" */ './views/Carrito.vue'),
    },
    {
      path: '/producto/:producto/:id',
      name: 'producto',
      component: () => import(/* webpackChunkName: "about" */ './views/Productos.vue'),
      props: (route) => ({ producto: route.params.producto, id: route.params.id, query: route.query.pro }) 
    },
    {
      path: '/login',
      name: 'login',
      component: () => import(/* webpackChunkName: "about" */ './views/Login.vue')
    },
    {
      path: '/perfil',
      name: 'perfil',
      component: () => import(/* webpackChunkName: "about" */ './views/Perfil.vue'),
      props: true
    },
    {
      path: '/pagar',
      name: 'pagar',
      component: () => import(/* webpackChunkName: "about" */ './views/Pago.vue'),
      props: (route) => ({ token: route.query.token,lal: route.query.lal }) 
    },
    {
      path: '/sobre',
      name: 'sobre',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ './views/Sobre.vue')
    },
    {
      path: '*',
      name: 'nofound',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ './views/Page-404.vue')
    }
  ]
})
