import Vue from 'vue'
import Vuex from 'vuex'
import './plugins/vuetify'
import App from './App.vue'
import router from './router'
import 'roboto-fontface/css/roboto/roboto-fontface.css'
import '@fortawesome/fontawesome-free/css/all.css'
import { store } from './store'
import VueTranslate from 'vue-translate-plugin';

Vue.config.productionTip = false
Vue.use(Vuex)
Vue.use(VueTranslate);

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')